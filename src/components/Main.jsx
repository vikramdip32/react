import Home from './home/Home';
import React, { useContext, createContext, useState } from "react";
import { Header } from './common/Header'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import About from './about/About';
import Login from './auth/Login';
import Products from './products/Products';
export default function Main() {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/about">
        <About />
      </Route>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/products">
        <Products />
      </Route>
    </Switch>);
}
function PrivateRoute({ children }) {
  let auth = false;
  return (
    <Route
      render={({ location }) =>
        auth ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
            }}
          />
        )
      }
    />
  );
}