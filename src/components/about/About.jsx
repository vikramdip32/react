import React from 'react'

function About() {

    return (
        <>
            <div className="page-heading about-heading header-text">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="text-content">
                                <h4>about us</h4>
                                <h2>our company</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="best-features about-features">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="section-heading">
                                <h2>Our Background</h2>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="right-image">
                                <img src="assets/images/feature-image.jpg" alt="" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="left-content">
                                <h4>Who we are &amp; What we do?</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed voluptate nihil eum consectetur similique? Consectetur, quod, incidunt, harum nisi dolores delectus reprehenderit voluptatem perferendis dicta dolorem non blanditiis ex fugiat. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<br /><br />Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, consequuntur, modi mollitia corporis ipsa voluptate corrupti eum ratione ex ea praesentium quibusdam? Aut, in eum facere corrupti necessitatibus perspiciatis quis.</p>
                                <ul className="social-icons">
                                    <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i className="fa fa-behance"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="team-members">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="section-heading">
                                <h2>Our Team Members</h2>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="team-member">
                                <div className="thumb-container">
                                    <img src="assets/images/team_01.jpg" alt="" />
                                    <div className="hover-effect">
                                        <div className="hover-content">
                                            <ul className="social-icons">
                                                <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i className="fa fa-behance"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="down-content">
                                    <h4>Johnny William</h4>
                                    <span>CO-Founder</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing itaque corporis nulla.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="team-member">
                                <div className="thumb-container">
                                    <img src="assets/images/team_02.jpg" alt="" />
                                    <div className="hover-effect">
                                        <div className="hover-content">
                                            <ul className="social-icons">
                                                <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i className="fa fa-behance"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="down-content">
                                    <h4>Karry Pitcher</h4>
                                    <span>Product Expert</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing itaque corporis nulla.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="team-member">
                                <div className="thumb-container">
                                    <img src="assets/images/team_03.jpg" alt="" />
                                    <div className="hover-effect">
                                        <div className="hover-content">
                                            <ul className="social-icons">
                                                <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i className="fa fa-behance"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="down-content">
                                    <h4>Michael Soft</h4>
                                    <span>Chief Marketing</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing itaque corporis nulla.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="team-member">
                                <div className="thumb-container">
                                    <img src="assets/images/team_04.jpg" alt="" />
                                    <div className="hover-effect">
                                        <div className="hover-content">
                                            <ul className="social-icons">
                                                <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i className="fa fa-behance"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="down-content">
                                    <h4>Mary Cool</h4>
                                    <span>Product Specialist</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing itaque corporis nulla.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="team-member">
                                <div className="thumb-container">
                                    <img src="assets/images/team_05.jpg" alt="" />
                                    <div className="hover-effect">
                                        <div className="hover-content">
                                            <ul className="social-icons">
                                                <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i className="fa fa-behance"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="down-content">
                                    <h4>George Walker</h4>
                                    <span>Product Photographer</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing itaque corporis nulla.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="team-member">
                                <div className="thumb-container">
                                    <img src="assets/images/team_06.jpg" alt="" />
                                    <div className="hover-effect">
                                        <div className="hover-content">
                                            <ul className="social-icons">
                                                <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i className="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i className="fa fa-behance"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="down-content">
                                    <h4>Kate Town</h4>
                                    <span>General Manager</span>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing itaque corporis nulla.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div className="services">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="service-item">
                                <div className="icon">
                                    <i className="fa fa-gear"></i>
                                </div>
                                <div className="down-content">
                                    <h4>Product Management</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur an adipisicing elit. Itaque, corporis nulla at quia quaerat.</p>
                                    <a href="#" className="filled-button">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="service-item">
                                <div className="icon">
                                    <i className="fa fa-gear"></i>
                                </div>
                                <div className="down-content">
                                    <h4>Customer Relations</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur an adipisicing elit. Itaque, corporis nulla at quia quaerat.</p>
                                    <a href="#" className="filled-button">Details</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="service-item">
                                <div className="icon">
                                    <i className="fa fa-gear"></i>
                                </div>
                                <div className="down-content">
                                    <h4>Global Collection</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur an adipisicing elit. Itaque, corporis nulla at quia quaerat.</p>
                                    <a href="#" className="filled-button">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div className="happy-clients">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="section-heading">
                                <h2>Happy Partners</h2>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="owl-clients owl-carousel">
                                <div className="client-item">
                                    <img src="assets/images/client-01.png" alt="1" />
                                </div>

                                <div className="client-item">
                                    <img src="assets/images/client-01.png" alt="2" />
                                </div>

                                <div className="client-item">
                                    <img src="assets/images/client-01.png" alt="3" />
                                </div>

                                <div className="client-item">
                                    <img src="assets/images/client-01.png" alt="4" />
                                </div>

                                <div className="client-item">
                                    <img src="assets/images/client-01.png" alt="5" />
                                </div>

                                <div className="client-item">
                                    <img src="assets/images/client-01.png" alt="6" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
export default About;