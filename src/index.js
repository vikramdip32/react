import React from 'react';
import reactDom from 'react-dom';
import Header from './components/common/Header';
import { Router, Route, Switch, BrowserRouter } from "react-router-dom";
import Footer from './components/common/Footer';
import Main from './components/Main';




reactDom.render(
  <>
    <BrowserRouter>
      <Header />
      <Main />
      <Footer />
    </BrowserRouter>

  </>
  ,
  document.getElementById('root')
);


